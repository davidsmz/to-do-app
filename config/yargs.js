const description = {
  demand: true,
  alias: 'd',
  desc: 'Description of tasks'
};

const complete = {
  alias: 'c',
  default: true,
  desc: 'Mark a task complete or pendent'
};

const argv = require('yargs')
                  .command('create', 'Create a task to do', {
                    description
                  })
                  .command('update', 'Update status of task', {
                    description,
                    complete
                  })
                  .command('list', 'show list of tasks', {
                    complete
                  })
                  .command('delete', 'Delete a task', {
                    description
                  })
                  .help()
                  .argv;

module.exports = {
  argv
};