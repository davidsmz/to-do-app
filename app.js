const argv = require('./config/yargs').argv;
const task = require('./to-do/task');
const command = argv._[0];

switch (command) {
  case 'create':
    let taskCreated = task.create(argv.description);
    console.log(taskCreated);
    break;
  case 'update':
    let updated = task.update(argv.description, argv.complete);
    console.log(updated);
    break;
  case 'list':
    task.list();
    break;
  case 'delete':
    task.deleteTask(argv.description);
    break;
  default:
    console.log('Command invalid');
    break;
}