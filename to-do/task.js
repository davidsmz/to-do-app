const fs = require('fs');
const colors = require('colors');
const argv = require('../config/yargs').argv;

let listTask = [];

// function for save task in DB
let saveDB = () => {
  let data = JSON.stringify(listTask);
  fs.writeFile('db/tasks.json', data, error => {
    if (error) {
      throw new Error('Failed to save tasks in database')
    }
  })
};

// function for upload tasks
let uploadDB = () => {

  try {
    listTask = require('../db/tasks');
  }catch (e) {
    listTask = [];
  }
};

let create = (description) => {
  uploadDB();
  let task = {
    description,
    complete: false
  };

  listTask.push(task);
  saveDB();
  return task;
};

// update status of task
let update = (description, complete = true) => {
  uploadDB();
  let index = listTask.findIndex(task => {
    return task.description === description;
  });

  if (index >= 0) {
    listTask[index].complete = complete;
    saveDB();
    return true;
  }else {
    return false;
  }

};

const list = () => {
  uploadDB();
  if (argv.complete === 'true') {
    listTask = listTask.filter(task => {
      return task.complete === true
    });
  }

  if (argv.complete === 'false') {
    listTask = listTask.filter(task => {
      return task.complete === false
    });
  }

  for (let task=0; task < listTask.length; task++) {
    console.log(`====Task ${task + 1}====`.green);
    console.log('Description: ', listTask[task].description);
    console.log('Complete: ', listTask[task].complete);
    console.log('=============='.blue);
  }
};

const deleteTask = (description) => {
  uploadDB();
  let newListTask = listTask.filter(task => {
    return task.description !== description;
  });

  if (newListTask.length === listTask) {
    return false;
  } else {
    listTask = newListTask;
    saveDB();
    return true;
  }
};

module.exports = {
  create,
  update,
  list,
  deleteTask
};